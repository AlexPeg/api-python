from flask import Flask,make_response
from flask_mongoengine import MongoEngine
from api_constants import mongodb_password

# Start app
app = Flask(__name__)
database_name ="API"
# DB_URI ="mongodb+srv://ALEX:{}@cluster0.6yokf.mongodb.net/{}?retryWrites=true&w=majority".format(
#     mongodb_password,database_name
# )
DB_URI = "mongodb+srv://ALEX:dNlpSbwbVogPgJgd@cluster0.6yokf.mongodb.net/API?retryWrites=true&w=majority"
app.config["MONGODB_URI"] = DB_URI
db = MongoEngine()
db.init_app(app)

if __name__ == "__main__":
    app.run()
    
class Veilles(db.Document):
  veille_id = db.IntField()
  title = db.StringField()
  description = db.StringField()
  links = db.StringField()
  author = db.StringField()
  tag = db.StringField()

 #Convert to json
  def to_json(self):
    return {
      "veille_id": self.veille_id,
      "title": self.title,
      "description": self.description,
      "links": self.links,
      "author": self.author,
      "tag": self.tag,
    }

#Post a document manually
@app.route('/api/db_populate', methods=['POST'])
def db_populate():
  veille1 = Veilles(veille_id=1, title="Veille importante", description="BLABLA", links="https://Google.com/", author="Alex", tag="#veille")
  veille2 = Veilles(veille_id=2, title="Veille du futur", description="Bloup", links="https://Google.com/", author="Dan", tag="#veille")
  veille1.save()
  veille2.save()
  return make_response("", 201)

#Get all topics and post a document
@app.route('/api/v1/topics', methods=['GET', 'POST'])
def api_veilles():
  if request.method == "GET":
    veilles = []
    for veille in Veilles.objects:
      veilles.append(veille)
    return make_response(jsonify(veilles), 200)
  elif request.method == "POST":
    content = request.json
    veille = Veilles(veille_id=content['veille_id'],
    title=content['title'],
    description=content['description'],
    links=content['links'],
    author=content['author'],
    tag=content['tag'])
    veille.save()
    return make_response("", 201)

#Get a document, put and delete 
@app.route('/api/v1/topics/<veille_id>', methods=['GET', 'PUT', 'DELETE'])
def api_each_veille(veille_id):
  if request.method == "GET":
    veille_obj = Veilles.objects(veille_id=veille_id).first()
    if veille_id:
      return make_response(jsonify(veille_obj.to_json()), 200)
    else:
      return make_response("", 404)
  elif request.method == "PUT":
    content = request.json
    veille_obj = Veilles.objects(veille_id=veille_id).first()
    veille_obj.update(veille_id=content['veille_id'],
    title=content['title'],
    description=content['description'],
    links=content['links'],
    author=content['author'],
    tag=content['tag'])
    return make_response("", 204)
  elif request.method == "DELETE":
    veille_obj = Veilles.objects(veille_id=veille_id).first()
    veille_obj.delete()
    return make_response("", 204)
