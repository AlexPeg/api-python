import mongoengine as db
from api_constants import mongo_password

password = mongo_password
database_name = "APIPython"
DB_URI ="mongodb+srv://ALEX:{}@cluster0.6yokf.mongodb.net/{}?retryWrites=true&w=majority".format(
    password,database_name
)
db.connect(host=DB_URI)

#Create document
class Veille(db.Document):
  veille_id = db.IntField()
  title = db.StringField()
  description = db.StringField()
  links = db.StringField()
  author = db.StringField()
  tag = db.StringField()

  #Convert to json
  def to_json(self):
    return {
      "veille_id": self.veille_id,
      "title": self.title,
      "description": self.description,
      "links": self.links,
      "author": self.author,
      "tag": self.tag,
    }

"""print("\nCreer une veille")
veille1 = Veille(
      veille_id=1,
      title="Veille python",
      description="Python le langage du futur", 
      links="https://Google.com/", 
      author="orel",
      tag="#veille")
  
veille1.save()"""
 

"""print("\nRecuperer une vielle")"""
veille = Veille.objects(veille_id=1).first()
"""print(veille.to_json())"""

"""print("\nMettreà jour une veille")
veille.update(
    title="Veille Swift",
    description="Swift le futur",
    links="https://Google.com/", 
    author="Alex",
    tag="#veille"
)
print(veille.to_json())"""

print("\nAjouter une autre veille")
veille2 = Veille(
      veille_id=2,
      title="Veille JS",
      description="JSle langage du futur", 
      links="https://Google.com/", 
      author="mr.robot",
      tag="#veille")
  
veille2.save()

print("\nRecuperer toutes les veilles")
veilles= []
for veille in Veille.objects():
    veilles.append(veille.to_json())
print(veilles)

print("\nSupprimer une veille")
veille= Veille.objects(veille_id=2).first()
veille.delete()